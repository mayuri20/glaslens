/*----------------------------------------------------------------------
Copyright (c) 2000 Gipsysoft. All Rights Reserved.
File:	resstring.cpp
Owner:	russf@gipsysoft.com
Purpose:	Simple string resource loader
----------------------------------------------------------------------*/
#include "stdafx.h"
#include "ResString.h"
#include <Windows.h>
#ifndef _INC_TCHAR
	#include <TCHAR.H>
#endif	//	_INC_TCHAR

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

HINSTANCE CResString::h = GetModuleHandle( NULL );
HANDLE CResString::g_hHeap = GetProcessHeap();

CResString::CResString( UINT uStringID )
	: m_uStringID( uStringID )
//	, s( NULL )
//	, m_nLength( 0 )
{	if(!blank)
	{	blank = new char[s_size+1];
		blank[0] = 0;
		blank[s_size] = 0;
	}

}

char* CResString::blank;

CResString::~CResString()
{
#if 0
	if( s )
	{
		VERIFY( HeapFree( g_hHeap, HEAP_NO_SERIALIZE, s ) );
		s = NULL;
	}
#endif
}


char* CResString::c_str() const
{
	if(s.size())
		return (char*) s.c_str();
//	return c_str();
//}
//const LPCTSTR CResString::c_str() 
//
//	Retrun a pointer to the loaded string, if the string is not already
//	loaded then we will load it.
//
//	Can return "" if the string is not found.
//{

	//
	//	Generally, find the string in the resourcse, determine the size of the entire resource
	//	table and allocate enough memory for that. Then load the string. If all is well we will
	//	resize the string memory block so as not to waste space and then return the newly allocated memory.

	ASSERT( h );
#if 0
	HRSRC hRes = FindResource( h, MAKEINTRESOURCE( m_uStringID/16 +1 ), RT_STRING );
	if( !hRes )
	{
		TRACE(_T("Failed to load string with ID %d\n"), m_uStringID );
		return blank;
	}
	DWORD dwSize = SizeofResource( h, hRes );
	LPTSTR pcszString = static_cast<LPTSTR>( HeapAlloc( g_hHeap, HEAP_ZERO_MEMORY, dwSize * sizeof( TCHAR ) ) );
#endif
	if( !LoadString( h, m_uStringID, blank, s_size ) )
	{
		TRACE( _T("Resource not found as string or not loaded\n" ));
		//	Resource not found???
//		VERIFY( HeapFree( g_hHeap, HEAP_NO_SERIALIZE, pcszString ) );
		return blank;
	}
#if 0
	m_nLength = lstrlen( pcszString );
	s = static_cast<LPTSTR>( HeapReAlloc( g_hHeap, HEAP_ZERO_MEMORY, pcszString, ( static_cast<DWORD>( m_nLength + 1 ) * sizeof(TCHAR) ) ) );
	if( !s )
	{
		s = pcszString;
	}
#endif
	s = blank;
	blank[0] = 0;
	return (char*) s.c_str();
}


int CResString::GetLength() const
//
//	return the length of the string in characters.
{
	if( !s.size() )
		c_str();

	return s.size();
}
